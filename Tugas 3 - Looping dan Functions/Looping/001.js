// LOOPING PERTAMA

var perulangan = 1;

console.log('LOOPING PERTAMA')
while(perulangan <= 20){
    if (perulangan % 2 == 0){
        console.log(perulangan + ' - I love coding')
    }
    perulangan ++;
}

// LOOPING KEDUA

var kedua = 20;
console.log('LOOPING KEDUA');
while(kedua >=1){
    if(kedua % 2 == 0){
        console.log (kedua + ' - I will become a mobile developer')
    }
    kedua --;
}


