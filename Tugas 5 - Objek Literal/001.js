console.log('Cara Pertama')

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]

function arrayToObject(arr) {
    if (arr.length === 0) {
        console.log('');
    }
    var nama = {}
    for(var i = 0; i < arr.length; i++){
        nama.firstName = arr[i][0];
        nama.lastName = arr[i][1];
        nama.gender = arr[i][2];

        var now = new Date().getFullYear()
        if (arr[i][3] == undefined || arr[i][3] > now) {
            nama.age = 'Invalid Birth Year';
        } else {
            nama.age = now - arr[i][3];
        }
        var tagName = (i+1)+'. '+nama.firstName+ ' ' +nama.lastName +': ';
        console.log(tagName);
        console.log(nama);
    }
}

arrayToObject(people)
arrayToObject(people2)


// cara lain

console.log('Cara Ke Dua')

function arrayToObject(arr) {
    // Code di sini 
    if (arr.length === 0) {
        console.log('');
     }

    for(var j= 0; j <  arr.length; j++){
        var now = new Date()
        var tahun = now.getFullYear() 

        var peopleArr = arr[j];
        var objPeople = {
            firstName : peopleArr[0],
            lastName : peopleArr[1],
            ege : peopleArr[2],
        }

        if(!peopleArr[3] || peopleArr[3] > tahun){
            objPeople.age ="Invalid Birth Year"
        }else{
            objPeople.age = tahun - peopleArr[3]
        }

        var fullName = objPeople.firstName +" "+ objPeople.lastName
        console.log(`${j+ 1}. ${fullName} : `, objPeople)
    }
}

arrayToObject(people)
arrayToObject(people2)