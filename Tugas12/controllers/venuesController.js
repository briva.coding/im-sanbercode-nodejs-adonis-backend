// import model
const e = require('express');
const {Venues} = require('../models');

class venueController {
    static async store (req, res){

        try{
            let name = req.body.name
            let address = req.body.address
            let phone = req.body.phone
    
            const newVenues = await Venues.create({
                name : name,
                address : address,
                phone : phone
            })
    
            res.status(200).json({
                status : "Sukses",
                message : "Berhasil Di tambahkan"
            })

        } catch (error){
            res.status(401).json({
                status : "Failed",
                message : "Gagal Menyimpan data",
                msg : error.errors.map((e) => e.message)
            })
        }


    
    }

}

module.exports = venueController;