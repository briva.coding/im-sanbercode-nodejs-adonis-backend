var express = require('express');
const { route } = require('.');
var router = express.Router();

const venueController = require('../controllers/venuesController');

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

router.venues("/", venueController.index);


module.exports = router;
