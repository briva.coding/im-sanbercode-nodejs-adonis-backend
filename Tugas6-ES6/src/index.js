import {sapa, convert} from './libs/001'

const myArgs = process.argv.slice(2);
const command = myArgs[0]

switch (command){
    case 'sapa' :
        let nama1 = myArgs[1];
        console.log(sapa(nama1))
        break;

    case 'convert' :
        const params = myArgs.slice(1);
        let [nama, domisili, umur] = params

        console.log(convert(nama, domisili, umur))
        break;

    case 'checkScore' :
        break;
    default :
        break;
}