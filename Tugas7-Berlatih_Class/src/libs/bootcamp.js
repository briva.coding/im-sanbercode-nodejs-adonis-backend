class Bootcamp {
    constructor(name, level, instructor){
        this._name = name
        this._level = level
        this._instructor = instructor
        this._students = []
    }

    get name(){
        return this._name
    }

    set name (strname){
        this._name = strname
    }

    get level(){
        return this._level
    }

    set level (strlevel){
        this._level = strlevel
    }

    get instructor(){
        return this._instructor
    }

    set instructor (strinstructor){
        this._instructor = strinstructor
    }

    get students (){
        return this._students
    }
    addStudents(objStudent){
        this._students.push(objStudent)
    }
}

export default Bootcamp;