import Bootcamp from "./bootcamp"

class kelas{
    constructor(name){
        this._name = name
        this._Bootcamp = []
    }

    get name(){
        return this._name
    }

    set name(name){
        this._name = name
    }

    get _Bootcamp(){
        return this._Bootcamp
    }

    createClass(name, level, instructor ){
        let newBootcamp = new Bootcamp (name, level, instructor)
        this._Bootcamp.push(newBootcamp)
    }
        
}

export default kelas


