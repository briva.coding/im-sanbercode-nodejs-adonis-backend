const fs = require('fs')
const fsPromises = require('fs/promises')

const path = 'data.json'

class registerController {
    static registers(req, res){
        // res.status(200).json({message: "Testing"})
        fs.readFile(path, (err, data)=>{
            if(err){
                res.status(400).json({
                    error :"data tidak terbaca"
                })
            }else{
                let existingData = JSON.parse(data)

                let {users} = existingData

                let {name, role, password} = req.body
                let newRegister = {name, role, password, isLogin:false}

                users.push(newRegister)

                let newData = {...existingData, users}

                fs.writeFile(path, JSON.stringify(newData), (err)=>{
                    if(err){
                        res.status(400).json({error : "data gagal register"})
                    }else{
                        res.status(201).json({
                            message : "Berhasil Register"
                        })
                    }
                })
            }

        })



    }

    static findAll(req, res){
        fs.readFile(path,(err, data)=>{
            if(err){
                res.status(400).json({error : "data baca data"})
            }else {
                let realData = JSON.parse(data)
                res.status(200).json({
                    message : "Berhasil get karyawan",
                    data : realData.users
                })
            }
        })
    }

    static login(req, res){
        fsPromises.readFile(path)
        .then((data)=>{
            let employes = JSON.parse(data);

            let {users} = employes

            let {name, password} = req.body

            let indexEmp = users.findIndex(user => users.name == name)
            // console.log(indexEmp)

            if(indexEmp == -1){
                res.status(401).json({error : "Data tidak ditemukan"})
            }else {
                let employee = users[indexEmp]

                if(employee.password == password){
                    employee.isLogin =true;

                    users.splice(indexEmp, 1, employee)
                    return fsPromises.writeFile(path, JSON.stringify(employes))
                }else {
                    res.status(400).json({error : "Password Salah"})
                }
            }
        })
        .then(()=>{
            res.status(200).json({message: "Berhasil Login"})
        })
        .catch((err)=>{
            console.log(err)
        })
    }
}

module.exports = registerController