var express = require('express');
var router = express.Router();

const userController = require('../controllers/registerController')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/register', userController.registers)
router.get('/karyawan', userController.findAll)
router.post('/login', userController.login)

module.exports = router;
