import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'
import UserValidator from 'App/Validators/UserValidator'

export default class AuthController {
    public async register ({response, request}:HttpContextContract){
        try {
            const data = await request.validate(UserValidator)
            await User.create(data)

            return response.created({
                message: "Register Berhasil"
            })
        } catch (error) {
            return response.unprocessableEntity({
                message:error
            })
        }
    }

    public async login ({ auth, request, response } : HttpContextContract) {
        try {
            // pakai userSchema karena yg dipakai hanya email dan password
            const userSchema = schema.create({
                email: schema.string(),
                password: schema.string()
            })
            await request.validate({schema:userSchema})
            const email = request.input('email')
            const password = request.input('password')

            const token = await auth.use("api").attempt(email, password);

            return response.ok({
                message : "Berkasil Login",
                token
            })
        } catch (error) {
            console.log(error)
        }
      }



}
