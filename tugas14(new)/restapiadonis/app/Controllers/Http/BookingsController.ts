import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Booking from 'App/Models/Booking';

export default class BookingsController {
  public async index({response}: HttpContextContract) {
    try {
      let booking = await Booking.all();

      response.ok({
        message : "Booking berhasil ditampilkan",
        data : booking
      })
      
    } catch (error) {
      response.badRequest({
        message : "Data Booking Gagal Ditampilkan"
      })
    }
  }

 

  public async store({request, response}: HttpContextContract) {
    try {
      await Booking.create({
        user_id : request.input('user_id'),
        play_date_start : request.input('play_date_start'),
        play_date_end : request.input('play_date_end'),
        field_id : request.input('field_id')
      })
     
      response.created({
        message : "Data Booking Berhasil Disimpan"
      })
      
    } catch (error) {
        response.badRequest({
        message : "Data Booking Gagal Disimpan"
      })
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {
      const dataDetailBooking = await Booking.findOrFail(params.id)
      response.ok({
        message : "Data Berhasil Ditampilkan",
        data : dataDetailBooking
      })
    } catch (error) {
        response.badRequest({
        message : "Data Gagal Ditampilkan"
      })
    }
  }

  // public async edit({}: HttpContextContract) {}

  public async update({request, response, params}: HttpContextContract) {
    try {
      const Bookings = await Booking.findOrFail(params.id)
      Bookings.user_id = request.input('user_id')// Luxon dateTime is used
      Bookings.play_date_start = request.input('play_date_start')
      Bookings.play_date_end = request.input('play_date_end')
      Bookings.field_id = request.input('field_id')

      await Bookings.save()


      response.ok({
        message : "Data Booking Berhasil Di Update"
      })
      
    } catch (error) {
        response.badRequest({
        message : "Data Booking Gagal Diupadate"
      })
    }
  }

  public async destroy({response,params}: HttpContextContract) {
    try {
      const bookingDelete = await Booking.findOrFail(params.id)
      await bookingDelete.delete()

      response.ok({
        message : "Data Booking berhasil dihapus"
      })
      
    } catch (error) {
      response.badRequest({
        message : "Data Booking Gagal Dihapus"
      })
    }
  }
}
