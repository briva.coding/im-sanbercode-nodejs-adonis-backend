
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
// import Database from '@ioc:Adonis/Lucid/Database'
import Field from 'App/Models/Field'


export default class FieldsController {
  public async index({response}: HttpContextContract) {
    try {
      // const DataFields = await Database
      // .from ('fields')
      // .select('*')

      let fields = await Field.all();

      response.ok({
        message : "Fields berhasil ditampilkan",
        data : fields
      })


     
      
    } catch (error) {
        response.badRequest({
        message : "Data Gagal Ditampilkan"
      })
    }
  }

  public async store({request, response}: HttpContextContract) {
    try {
      // await Database
      // .table('fields')
      // .insert({
      //   name : request.input('name'),
      //   type : request.input('type'),
      //   venue_id : request.input('venue_id')
      // })

      await Field.create({
        name : request.input('name'),
        type : request.input('type'),
        venue_id : request.input('venue_id')
      })
     
      response.created({
        message : "Data Berhasil Disimpan"
      })
    } catch (error) {
      response.badRequest({
        message : "Data Gagal Disimpan"
      })
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {

      // const dataField = await Database
      // .from('fields')
      // .where('id',params.id)
      // .first()

      const dataDetailfield = await Field.findOrFail(params.id)
      response.ok({
        message : "Data Berhasil Ditampilkan",
        data : dataDetailfield
      })
      
    } catch (error) {
        response.badRequest({
        message : "Data Gagal Ditampilkan"
      })
    }
  }

  public async update({request, response, params}: HttpContextContract) {
    try {
      // await Database
      // .from('fields')
      // .where('id', params.id)
      // .update({
      //   name : request.input('name'),
      //   type : request.input('type'),
      //   venue_id : request.input('venue_id')
      // })
      const Fields = await Field.findOrFail(params.id)
      Fields.name = request.input('name')// Luxon dateTime is used
      Fields.type = request.input('type')
      Fields.venue_id = request.input('venue_id')

      await Fields.save()


      response.ok({
        message : "Data Field Berhasil Di Update"
      })
    } catch (error) {
      response.badRequest({
        message : "Data Field Gagal Diupadate"
      })
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      // await Database
      // .from('fields')
      // .where('id', params.id)
      // .delete()

      const fieldDelete = await Field.findOrFail(params.id)
      await fieldDelete.delete()

      response.ok({
        message : "Data Field berhasil dihapus"
      })
    } catch (error) {
      response.badRequest({
        message : "Data Field Gagal Dihapus"
      })
    }
  }
}
