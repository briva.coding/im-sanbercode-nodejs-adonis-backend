import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
// import VenueValidator from 'App/Validators/VenueValidator'
// import Database from '@ioc:Adonis/Lucid/Database'
import Venue from 'App/Models/Venue'

export default class Venues2sController {
  public async index({response}: HttpContextContract) {
    try {
      // jika menggunakan query builder
          // const DataVenues = await Database
          // .from('venues')
          // .select('*')
      

          // response.ok({
          //   message : "Data Berhasil Ditampilkan",
          //   data : DataVenues
          // })
      // end

      // menggunakan orm lucid
          let venuest = await Venue.all();

          response.ok({
            message : "venuee berhasil ditampilkan",
            data : venuest
          })



      // end



    } catch (error) {
      response.badRequest({
        message : "Data Gagal Ditampilkan"
      })
    }
  }

// tambah data
  public async store({response, request}: HttpContextContract) {
    try {
      // jika menggunakan query builder
          // await Database
          // .table('venues')
          // .insert({
          //   name : request.input('name'),
          //   address : request.input('address'),
          //   phone : request.input('phone')
          // })

      // end
        // menggunakan ORM 
        await Venue.create({
          name: request .input('name'),
          address: request.input('address'),
          phone : request.input('phone'),
        })

        // end


      response.created({
        message : "Data Berhasil Disimpan"
      })
    } catch (error) {
      response.badRequest({
        message : "Data Gagal Disimpan"
      })
    }
    
    
   

  }
// tampil data
  public async show({response, params}: HttpContextContract) {
    try {
      // query builder
        // const venues2 = await Database
        //   .from('venues')
        //   .where('id',params.id)
        //   .first()
      // end
      // ORM
      const dataDetail = await Venue.findOrFail(params.id)
      // end
        response.ok({
          message : "Data Berhasil Ditampilkan",
          data : dataDetail
        })
      
    } catch (error) {
      response.badRequest({
        message : "Data Gagal Ditampilkan"
      })
    }
  }

// update data
  public async update({response, request, params}: HttpContextContract) {
    try {
      // query builder
          // await Database
          //   .from('venues')
          //   .where('id', params.id)
          //   .update({
          //     name : request.input('name'),
          //     address : request.input('address'),
          //     phone : request.input('phone')

          //   })
      // end
      // ORM
            const venue = await Venue.findOrFail(params.id)
            venue.name = request.input('name')// Luxon dateTime is used
            venue.address = request.input('address')
            venue.phone = request.input('phone')

            await venue.save()
      // end
      response.ok({
        message : "Data Berhasil Diupdate"
      })
    } catch (error) {
      response.badRequest({
        message : "Data Gagal Diupadate"
      })
    }
  }
// hapus data
  public async destroy({response, params}: HttpContextContract) {
    try {

      // by query builder
          // await Database
          // .from('venues')
          // .where('id',params.id)
          // .delete()

      // end
      // ORM
          const venueDelete = await Venue.findOrFail(params.id)
          await venueDelete.delete()
      // end

      response.ok({
        message : "Data berhasil dihapus"
      })
      
    } catch (error) {
      response.badRequest({
        message : "Data Gagal Dihapus"
      })
    }

  }
}
