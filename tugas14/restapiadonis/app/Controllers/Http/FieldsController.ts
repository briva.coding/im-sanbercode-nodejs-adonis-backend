
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'

export default class FieldsController {
  public async index({response}: HttpContextContract) {
    try {
      const DataFields = await Database
      .from ('fields')
      .select('*')

      response.ok({
        message : "Data Berhasil Ditampilkan",
        data : DataFields
      })
      
    } catch (error) {
        response.badRequest({
        message : "Data Gagal Ditampilkan"
      })
    }
  }

  public async store({request, response}: HttpContextContract) {
    try {
      await Database
      .table('fields')
      .insert({
        name : request.input('name'),
        type : request.input('type'),
        venue_id : request.input('venue_id')
      })
      response.created({
        message : "Data Berhasil Disimpan"
      })
    } catch (error) {
      response.badRequest({
        message : "Data Gagal Disimpan"
      })
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {

      const dataField = await Database
      .from('fields')
      .where('id',params.id)
      .first()

      response.ok({
        message : "Data Berhasil Ditampilkan",
        data : dataField
      })
      
    } catch (error) {
        response.badRequest({
        message : "Data Gagal Ditampilkan"
      })
    }
  }

  public async update({request, response, params}: HttpContextContract) {
    try {
      await Database
      .from('fields')
      .where('id', params.id)
      .update({
        name : request.input('name'),
        type : request.input('type'),
        venue_id : request.input('venue_id')
      })
      response.ok({
        message : "Data Berhasil Di Update"
      })
    } catch (error) {
      response.badRequest({
        message : "Data Gagal Diupadate"
      })
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      await Database
      .from('fields')
      .where('id', params.id)
      .delete()
      response.ok({
        message : "Data berhasil dihapus"
      })
    } catch (error) {
      response.badRequest({
        message : "Data Gagal Dihapus"
      })
    }
  }
}
