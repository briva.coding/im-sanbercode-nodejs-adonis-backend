import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
// import VenueValidator from 'App/Validators/VenueValidator'
import Database from '@ioc:Adonis/Lucid/Database'

export default class Venues2sController {
  public async index({response}: HttpContextContract) {
    try {

      const DataVenues = await Database
      .from('venues')
      .select('*')


      response.ok({
        message : "Data Berhasil Ditampilkan",
        data : DataVenues
      })
    } catch (error) {
      response.badRequest({
        message : "Data Gagal Ditampilkan"
      })
    }
  }

// tambah data
  public async store({response, request}: HttpContextContract) {
    try {
      await Database
      .table('venues')
      .insert({
        name : request.input('name'),
        address : request.input('address'),
        phone : request.input('phone')
      })
      response.created({
        message : "Data Berhasil Disimpan"
      })
    } catch (error) {
      response.badRequest({
        message : "Data Gagal Disimpan"
      })
    }
    
    
   

  }
// tampil data
  public async show({response, params}: HttpContextContract) {
    try {
      const venues2 = await Database
        .from('venues')
        .where('id',params.id)
        .first()

        response.ok({
          message : "Data Berhasil Ditampilkan",
          data : venues2
        })
      
    } catch (error) {
      response.badRequest({
        message : "Data Gagal Ditampilkan"
      })
    }
  }

// update data
  public async update({response, request, params}: HttpContextContract) {
    try {
     await Database
      .from('venues')
      .where('id', params.id)
      .update({
        name : request.input('name'),
        address : request.input('address'),
        phone : request.input('phone')

      })
      
      response.ok({
        message : "Data Berhasil Diupdate"
      })
    } catch (error) {
      response.badRequest({
        message : "Data Gagal Diupadate"
      })
    }
  }
// hapus data
  public async destroy({response, params}: HttpContextContract) {
    try {
      await Database
      .from('venues')
      .where('id',params.id)
      .delete()

      response.ok({
        message : "Data berhasil dihapus"
      })
      
    } catch (error) {
      response.badRequest({
        message : "Data Gagal Dihapus"
      })
    }

  }
}
