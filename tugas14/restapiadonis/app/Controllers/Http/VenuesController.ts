import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import BookingValidator from 'App/Validators/BookingValidator'
// import { schema, rules } from '@ioc:Adonis/Core/Validator'
import VenueValidator from 'App/Validators/VenueValidator'

export default class VenuesController {
    // public async index({response} : HttpContextContract){
    //     response.status(200).json({
    //         message : "test route index"
    //     })
    // }

    public async send({response, request}: HttpContextContract){
        try {
            // tidak diperlukan karena menggunakan validators
            // const newSend = schema.create({
            //     nama : schema.string(),
            //     alamat : schema.string(),
            //     phone : schema.string([
            //         rules.mobile()
            //       ])
            //   })

            // const velues = await request.validate({ schema: newSend })
            // end

            const newSend = await request.validate(VenueValidator)

              

               response.ok({
                message : newSend
               })

        } catch (error) {
            response.badRequest(error.messages)
        }
    }

    public async booking({response, request}: HttpContextContract){
        try {
            const newBooking = await request.validate(BookingValidator)
            response.ok({
                message : newBooking
            })
        } catch (error) {
            response.badRequest(error.messages)
        }
    }
}
